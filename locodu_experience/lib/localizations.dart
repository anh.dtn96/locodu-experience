/**
 * flutter pub pub run intl_translation:extract_to_arb --output-dir=lib/l10n lib/localizations.dart
 * 
 * flutter pub pub run intl_translation:generate_from_arb --output-dir=lib/l10n \ 
   --no-use-deferred-loading lib/localizations.dart lib/l10n/intl_*.arb
*/
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'l10n/messages_all.dart';

class AppLocalization{

  static AppLocalization of(BuildContext context) => Localizations.of(context, AppLocalization);

  static Future<AppLocalization> load(Locale locale){
    final String name = locale.countryCode == null? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

     return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return AppLocalization();
    });
  }

  String get signIn {
    return Intl.message('Sign In',name: 'signIn');
  }

  String get signUp {
    return Intl.message('Sign Up', name: 'signUp');
  }

}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalization> {
  
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'vi'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalization> load(Locale locale) {
    return AppLocalization.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old) {
    return false;
  }
}
