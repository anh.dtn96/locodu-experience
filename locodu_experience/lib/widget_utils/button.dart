
import 'package:flutter/cupertino.dart';
import 'package:locodu_experience/resource/diments.dart';
import 'package:locodu_experience/resource/styles.dart';

Widget button({String buttonText, Color textColor, Color buttonColor, void Function() onPressed}){
  return Expanded(
    child: Container(
      height: kButtonHeight,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(kBorderSize),
        color: buttonColor,
      ),
      child: CupertinoButton(
        child: Text(
          buttonText,
          style: styleTextButton.copyWith(color: textColor),
        ),
        onPressed: onPressed,
      ),
    ),
  );
}