import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:locodu_experience/splash_page.dart';
import 'extentions/timer.dart';

class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  Timer _timer;

  @override
  void initState() {
    _timer.start(seconds: 3, fun: goToSplashPage);
    super.initState();
  }

  void goToSplashPage() {
    Navigator.of(context)
        .push(CupertinoPageRoute(builder: (context) => SplashPage()));
  }

  @override
  void dispose() {
    _timer.stop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 280),
              child: Image.asset(
                "asset/images/title_app.png",
                width: 144,
                height: 62,
              ),
            ),
            SizedBox(
              child: CupertinoActivityIndicator(),
              width: 60,
              height: 60,
            ),
          ],
        ),
      ),
    );
  }
}
