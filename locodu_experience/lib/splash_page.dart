import 'package:flutter/cupertino.dart';
import 'package:locodu_experience/resource/colors.dart';
import 'package:locodu_experience/resource/diments.dart';
import 'package:locodu_experience/widget_utils/button.dart';
import 'localizations.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return CupertinoPageScaffold(
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 300,
              height: 324,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    child: SizedBox(
                      child: Image.asset("asset/images/logo.png"),
                      width: 300,
                      height: 324,
                    ),
                  ),
                  Positioned(
                    top: 132,
                    left: 78,
                    right: 78,
                    bottom: 132,
                    child: SizedBox(
                        height: 60,
                        width: 144,
                        child: Image.asset("asset/images/title_app.png")),
                  )
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.only(top: kSize64, left: kPaddingSize, right: kPaddingSize),
              child: Row(
                children: <Widget>[
                  button(
                      buttonText: AppLocalization.of(context).signIn,
                      textColor: kSecondColor,
                      buttonColor: kPrimaryColor,
                      onPressed: () {}),
                  SizedBox(
                    width: 8,
                  ),
                  button(
                      buttonText: AppLocalization.of(context).signUp,
                      textColor: kPrimaryColor,
                      buttonColor: kSecondColor,
                      onPressed: () {}),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
