const double kButtonHeight = 56;
const double kBorderSize = 4;
const double kPaddingSize = 16;
const double kSize64 = 64;