import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFD55B69);
const kSecondColor = Color(0xFFF2F2F2);
const kWhiteColor = Color(0xFFFFFFFF);
