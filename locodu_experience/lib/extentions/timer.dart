import 'dart:async';

extension TimerExtention on Timer {
  void start({int seconds,void Function() fun}){
    Timer(Duration(seconds: seconds), fun);
  }
  void stop(){
    this.cancel();
  }

}
